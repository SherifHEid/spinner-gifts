package com.ryan.luckywheel;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import io.realm.Realm;
import io.realm.RealmResults;
import rubikstudio.library.LuckyWheelView;
import rubikstudio.library.model.LuckyItem;
import rubikstudio.library.PielView;

public class MainActivity extends Activity {

    Realm realm;

    List<LuckyItem> data = getModelList();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        realm = Realm.getDefaultInstance();
        final LuckyWheelView luckyWheelView = findViewById(R.id.luckyWheel);


//        LuckyItem luckyItem1 = new LuckyItem();
//        luckyItem1.setTopText("100");
//        luckyItem1.setIcon( R.drawable.test1);
//        luckyItem1.setColor(0xffFFE0B2);
//        data.add(luckyItem1);
//
//        LuckyItem luckyItem2 = new LuckyItem();
//        luckyItem2.setTopText("200");
//        luckyItem2.setIcon( R.drawable.test2);
//        luckyItem2.setColor(0xffFFE0B2);
//        data.add(luckyItem2);
//
//        LuckyItem luckyItem3 = new LuckyItem();
//        luckyItem3.setTopText("300");
//        luckyItem3.setIcon(R.drawable.test3);
//        luckyItem3.setColor(0xffFFCC80);
//        data.add(luckyItem3);
//
//        //////////////////
//        LuckyItem luckyItem4 = new LuckyItem();
//        luckyItem4.setTopText("400");
//        luckyItem4.setIcon(R.drawable.test4);
//        luckyItem4.setColor(0xffFFF3E0);
//        data.add(luckyItem4);
//
        luckyWheelView.setData(getModelList());
        luckyWheelView.setRound(9);

        luckyWheelView.setLuckyWheelBackgrouldColor(0xff0000ff);
        luckyWheelView.setLuckyWheelTextColor(0xffcc0000);
//        luckyWheelView.setLuckyWheelCenterImage(getResources().getDrawable(R.drawable.icon));
//        luckyWheelView.setLuckyWheelCursorImage(R.drawable.ic_cursor);


        findViewById(R.id.play).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
 //              int index = getRandomIndex();
//                luckyWheelView.startLuckyWheelWithTargetIndex(index);
                Intent intent = new Intent(MainActivity.this,AddNewGift.class);
                startActivity(intent);
            }
        });

        luckyWheelView.setLuckyRoundItemSelectedListener(new LuckyWheelView.LuckyRoundItemSelectedListener() {
            @Override
            public void LuckyRoundItemSelected(int index) {
                Toast.makeText(getApplicationContext(), data.get(index).getTopText(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public List<LuckyItem> getModelList() {
        List<LuckyItem> list = new ArrayList<>();
        try {
            realm = Realm.getDefaultInstance();
            RealmResults<LuckyItem> results = realm
                    .where(LuckyItem.class)
                    .findAll();
            list.addAll(realm.copyFromRealm(results));
        } finally {
            if (realm != null) {
                realm.close();
            }
        }
        return list;
    }

    private int getRandomIndex() {
        Random rand = new Random();
        return rand.nextInt(data.size() - 1) + 0;
    }

    private int getRandomRound() {
        Random rand = new Random();
        return rand.nextInt(10) + 15;
    }
}
