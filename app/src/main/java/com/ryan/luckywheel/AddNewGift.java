package com.ryan.luckywheel;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.myhexaville.smartimagepicker.ImagePicker;
import com.myhexaville.smartimagepicker.OnImagePickedListener;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;

import io.realm.Realm;
import rubikstudio.library.model.LuckyItem;

public class AddNewGift extends Activity {

    Realm realm;
    LinearLayout giftPhoto;
    ImagePicker imagePicker;
    EditText giftName,giftQuantity;
    ImageView giftPhotoValue;
    Button addGift;
    File file;
    Bitmap selectedImage;
    byte[] byteArray;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_gift);
        initializeViews();
        setListener();
    }

    public void initializeViews() {
        realm = Realm.getDefaultInstance();
        giftPhoto = findViewById(R.id.gift_photo_layout);
        giftName = findViewById(R.id.gift_name);
        giftQuantity = findViewById(R.id.gift_quantity);
        giftPhotoValue = findViewById(R.id.gift_image_value);
        addGift = findViewById(R.id.btn_add_gift);
    }

    public void setListener() {
        addGift.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (checkInputs()){
                    writeToDatabase();
                }
            }
        });

        giftPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onPickImage();
            }
        });
    }

    public void onPickImage() {
        // Click on image button
        imagePicker = new ImagePicker(this, null, new OnImagePickedListener() {
            @Override
            public void onImagePicked(Uri imageUri) {

                file = imagePicker.getImageFile();
                selectedImage = BitmapFactory.decodeFile(imageUri.getPath());
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                selectedImage.compress(Bitmap.CompressFormat.PNG, 100, stream);
                byteArray = stream.toByteArray();
                Log.d("byteArrayAbove", "sherif " + selectedImage);

            }
        });
        imagePicker.setWithImageCrop(1, 1);
        imagePicker.choosePicture(true);
    }

    private boolean checkInputs() {
        if (giftName.getText().toString().trim().equals("")){
            giftName.setError("Please enter gift name");
            return false;
        }

        if (giftQuantity.getText().toString().trim().equals("")){
            giftQuantity.setError("Please enter gift quantity");
            return false;
        }
        if (giftPhotoValue.getDrawable() == null){
            Toast.makeText(getApplicationContext(), "Please choose gift photo", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    public void writeToDatabase(){
        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm bgRealm) {
                LuckyItem luckyItem = bgRealm.createObject(LuckyItem.class);
                luckyItem.setTopText(giftName.getText().toString());
                luckyItem.setSecondaryText(giftQuantity.getText().toString());
                luckyItem.setColor(0xffFFE0B2);
                Log.e("byteArray", "sherif " + byteArray);
                luckyItem.setIcon(byteArray);
            }
        }, new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {
                // Transaction was a success.
                Toast.makeText(getApplicationContext(), "Added Successfully, please add another gift", Toast.LENGTH_SHORT).show();
            }
        }, new Realm.Transaction.OnError() {
            @Override
            public void onError(Throwable error) {
                // Transaction failed and was automatically canceled.
                Toast.makeText(getApplicationContext(), error.getMessage().toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        imagePicker.handleActivityResult(resultCode,requestCode, data);
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        imagePicker.handlePermission(requestCode, grantResults);
    }

    @Override
    protected  void onDestroy(){
        super.onDestroy();
        realm.close();
    }
}
