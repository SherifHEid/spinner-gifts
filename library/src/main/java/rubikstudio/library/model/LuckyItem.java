package rubikstudio.library.model;

import io.realm.RealmObject;

/**
 * Created by kiennguyen on 11/5/16.
 */

public class LuckyItem extends RealmObject {
    private String topText;
    private String secondaryText;
    private int secondaryTextOrientation;
    private byte[] icon;
    private int color;

    public String getTopText() {
        return topText;
    }

    public void setTopText(String topText) {
        this.topText = topText;
    }

    public String getSecondaryText() {
        return secondaryText;
    }

    public void setSecondaryText(String secondaryText) {
        this.secondaryText = secondaryText;
    }

    public byte[] getIcon() {
        return icon;
    }

    public void setIcon(byte[] icon) {
        this.icon = icon;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return "LuckyItem{" +
                "topText='" + topText + '\'' +
                ", secondaryText='" + secondaryText + '\'' +
                ", secondaryTextOrientation=" + secondaryTextOrientation +
                ", icon=" + icon +
                ", color=" + color +
                '}';
    }
}
